export interface SuffixTrie {
    add(text: string, value: string): void;
    query(text: string): Array<string>;
    stringify(): string;
    parse(data: string): SuffixTrie;
}

export function createSuffixTrie(): SuffixTrie; 