import { createSuffixTrie } from '.';
import { readFileSync } from 'fs';

const trie = createSuffixTrie();

// const data = readFileSync('./trie.json', 'utf8');
// trie.parse(data);
trie.add('HELLO', 'hellovalue');

const ans = trie.query('h');

console.log(ans);