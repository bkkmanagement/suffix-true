
const createTrieNode = (): TrieNode => {
    let edges = new Map<string, TrieNode>();
    let results = new Set<string>();
    return  {
        edges,
        results,
        add(text: string, value: string) {
            if (process.env.TEST) {
                console.log(text);
            }
            if (text.length === 0) {
                results.add(value);
            } else {
                const char = text[0];
                if (edges.has(char)) {
                    edges.get(char).add(text.slice(1), value);
                } else {
                    const nNode = createTrieNode();
                    edges.set(char, nNode);
                    nNode.add(text.slice(1), value);
                }
            }
        },
        query(text: string, set: Set<string>) {
            if (process.env.TEST) {
                console.log(edges);
            }
            if (text.length === 0) {
                // Include current node result if query already reach its end
                for(const result of results) {
                    set.add(result);
                }
                for(const [char, edge] of edges) {
                    edge.query('', set);
                }
            } else {
                const char = text[0];
                if (edges.has(char)) {
                    edges.get(char).query(text.slice(1), set);
                }
            }
            return set;
        },
        toData(): JSONTrieNodeData {
            const edgesData = [...edges.entries()].reduce<JSONTrieNodeData['edges']>((prev, [char, edge]) => {
                prev[char] = edge.toData();
                return prev;
            }, {});
            return {
                edges: edgesData,
                results: [...results.values()],
            };
        },
        parse(data) {
            results = new Set(data.results);
            edges = new Map();
            for(const key of Object.keys(data.edges)) {
                const trieNode = createTrieNode();
                trieNode.parse(data.edges[key]);
                edges.set(key, trieNode);
            }
            return this;
        }
    };
};

export const createSuffixTrie = (): SuffixTrie => {
    const rootNode = createTrieNode();
    return {
        add(text, value) {
            let toAdd = text.toLowerCase();
            while (toAdd.length > 0) {
                rootNode.add(toAdd, value);
                toAdd = toAdd.slice(1);
            }
        },
        query(text){
            return [...rootNode.query(text.toLowerCase(), new Set()).values()];
        },
        stringify() {
            return JSON.stringify(rootNode.toData());
        },
        parse(data: string) {
            const parsedData = JSON.parse(data);
            rootNode.parse(parsedData);
            // TO-DO: Parser
            return null;
        }
    };
};




export interface JSONTrieNodeData {
    edges: {[char: string]: JSONTrieNodeData};
    results: Array<string>;
}


export interface TrieNode {
    edges: Map<string, TrieNode>;
    results: Set<string>;
    add(text: string, value: string): void;
    query(text: string, set: Set<string>): Set<string>;
    toData(): JSONTrieNodeData;
    parse(obj: JSONTrieNodeData): TrieNode;
}


export interface SuffixTrie {
    add(text: string, value: string): void;
    query(text: string): Array<string>;
    stringify(): string;
    parse(data: string): SuffixTrie;
}